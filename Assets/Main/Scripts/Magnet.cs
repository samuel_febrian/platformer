﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class Magnet : PickableItem {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	protected override void Pick() 
	{
		// we send a new points event for the GameManager to catch (and other classes that may listen to it too)
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		player.GetComponent<Runner> ().StartCoinMagnet ();
	}
}
