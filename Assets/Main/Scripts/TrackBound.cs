﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackBound : MonoBehaviour {

	// Use this for initialization
	TrackManager trackManager;

	void Start () {
		trackManager = FindObjectOfType<TrackManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.layer == 9) {
			trackManager.ReturnTrackToFirstPlace ();
		}
	}
}
