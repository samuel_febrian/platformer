﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Track : MonoBehaviour {

	TrackManager trackManager;
	public Transform startPoint;
	public Transform endPoint;

	// Use this for initialization
	void Start () {
		trackManager = FindObjectOfType<TrackManager> ();

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.layer == 9) {
//			Debug.Log (other.name);
			trackManager.SpawnPlatform ();
		}
	}
}
