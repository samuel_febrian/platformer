﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;


public class MagnetRadius : MonoBehaviour {

	// Use this for initialization
	GameObject player ;

	void Start () {
		player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public virtual void OnTriggerEnter2D (Collider2D collider) 
	{
		
		if (collider.GetComponent<Coin> ()) {
			Coin coin = collider.GetComponent<Coin> ();
			//coin.GoToPlayer (player.transform.position);
			//coin.transform.Translate ((player.transform.position-transform.position)*Time.deltaTime*5, Space.World);

			StartCoroutine (CoinGoTo (coin,player.transform));
		}
	}

	IEnumerator CoinGoTo(Coin coin, Transform target){
		while ((coin.transform.position-player.transform.position).magnitude > 0.01f){
			coin.transform.Translate ((target.position-coin.transform.position)*Time.deltaTime*10,Space.World);
			yield return new WaitForEndOfFrame ();
		}
		StopAllCoroutines ();
	}
}
