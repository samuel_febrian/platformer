﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class Runner : CharacterAbility {

	[HideInInspector]
	public enum PowerUp{
		magnetCoin,
		doubleCoin,
		doubleSpeed,
		none
	}

	[HideInInspector]
	public PowerUp currentPowerUp;



	CharacterRun characterRun;
	CharacterHorizontalMovement characterHorizontalMovement;
	TrackManager trackManager;
	GameObject AIFollower;
	bool enemyMoved = false;
	int coinEarned;
	int multiplier=1;
	Text coinEarnedText;


	[Header("Setting And References")]
	[Space(10)]
	[SerializeField] float speed;
	[SerializeField] GameObject magnetRadius;

	[Header("Special Ability Duration")]
	[Space(10)]
	[SerializeField] float magnetTimer=10f;
	[SerializeField] float doubleSpeedTimer=10f;
	[SerializeField] float doubleCoinTimer=10f;

	private float magnetDuration;

	// Use this for initialization
	void Start () {
		base.Initialization ();
		characterRun = GetComponent<CharacterRun> ();
		characterHorizontalMovement = GetComponent<CharacterHorizontalMovement> ();
		trackManager = FindObjectOfType<TrackManager> ();	
		AIFollower = FindObjectOfType<AIFollow> ().gameObject;

		coinEarnedText = FindObjectOfType<CoinsEarned> ().GetComponent<Text> ();

	}
	
	// Update is called once per frame
	void Update () {
		if (trackManager.hasBegin) {
			Walking ();
		}

		coinEarnedText.text = coinEarned.ToString ();

		if( coinEarned>=multiplier*100){
			multiplier += 1;
			coinEarned = 0;
		}
//		if (currentPowerUp == PowerUp.magnetCoin) {
//			if (Time.time-magnetDuration >= LevelManager.Instance.magnetTimer) {
//				StopCoinMagnet ();
//			}
//		}


	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.layer == 13) {
			Debug.Log ("GAME OVER");
		}

		if (other.GetComponent<Coin> ()) {
			Coin coin = other.GetComponent<Coin> ();

			//Add more coins since we dont want to make any change on original script
			int addMoreCoin = (multiplier - 1) * coin.PointsToAdd;
			MMEventManager.TriggerEvent (new CorgiEnginePointsEvent (PointsMethods.Add, addMoreCoin));


			AddCoinEarned ();
		}
	}

	void Walking(){
		if (_controller.State.IsCollidingRight) {
			_movement.ChangeState(CharacterStates.MovementStates.Idle);	

			return;
		}
		_controller.SetHorizontalForce(speed*16/100);
		_movement.ChangeState(CharacterStates.MovementStates.Walking);	
	}

	void StopAutoRun(){
		Vector3 test = new Vector3(Time.deltaTime*0,0,0);
		transform.Translate (test, Space.Self);
		Debug.Log ("Stopped");
	}

	public void StartCoinMagnet(){
		StartCoroutine ("IEStartCoinMagnet");
	}

	public void StopCoinMagnet(){
		magnetRadius.SetActive (false);
		currentPowerUp = PowerUp.none;
	}

	IEnumerator IEStartCoinMagnet(){
		magnetRadius.SetActive (true);
		currentPowerUp = PowerUp.magnetCoin;
		yield return new WaitForSeconds (magnetTimer);
		StopCoinMagnet ();
	}

	public void StartDoubleSpeed(){		
		StartCoroutine ("IEStartDoubleSpeed");
	}

	public void StopDoubleSpeed(){
		speed = speed / 2;
		currentPowerUp = PowerUp.none;
	}

	IEnumerator IEStartDoubleSpeed(){
		Debug.Log (speed);
		speed = speed * 2;
		currentPowerUp = PowerUp.doubleSpeed;
		Debug.Log (speed);
		yield return new WaitForSeconds (doubleSpeedTimer);
		StopDoubleSpeed ();

	}


	public void StartDoubleCoin(){		
		StartCoroutine ("IEStartDoubleCoin");
	}

	public void StopDoubleCoin(){
		currentPowerUp = PowerUp.none;
		Debug.Log ("Doubled Coin Stopped");
	}

	IEnumerator IEStartDoubleCoin(){
		currentPowerUp = PowerUp.doubleCoin;
		yield return new WaitForSeconds (doubleCoinTimer);
		StopDoubleCoin ();
	}

	public void AddCoinEarned(){
//		if (currentPowerUp == PowerUp.doubleCoin) {
//			coinEarned += 2;
//			Debug.Log ("Double Coin");
//		} else {
//			coinEarned += 1;
//		}
//
		coinEarned += (currentPowerUp == PowerUp.doubleCoin) ? 2 : 1;
	}

}
